import { Button, Row, Col } from 'react-bootstrap';


export default function Banner() {
	
	return (
			<Row>
				<Col className="p-4 text-center">
					<h1>Circle</h1>
					<h5>Where You Can Buy Anything</h5>
					<Button variant="primary">Puchase now!</Button>
				</Col>
			</Row>
			
		) 
};

