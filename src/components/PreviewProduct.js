import { Col, Card, Button, Container } from 'react-bootstrap';
import { useContext } from 'react'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function PreviewProduct(props) {
	const { user } = useContext(UserContext);
	// props is used here to get the data and breakPoint from the FeaturedCourses.js
	const { breakPoint, data } = props

	// Destructure the courses data
	const { _id, name, description, price } = data



return (

	<row className="p-4">
	    <Col>
			<Card style={{ width: '15rem', height: '20rem' }}>
				<Card.Body>
					<Card.Title>
					<Link to={`/products/${_id}`}>{name}</Link>
					</Card.Title>
					<Card.Text>{description}</Card.Text>
				</Card.Body>

				<Card.Footer>
					<h6 className="text-center">₱ {price}</h6>
				</Card.Footer>
				{
            		(user.id === null)
            		?
            		<Button as={Link} to="/login" className="ButtonColor text-white">Details</Button>
            		:
            		<Button as={Link} to={`/products/${_id}`} className="ButtonColor text-white">Details</Button>
          		}
		   	</Card>
		</Col>
	</row>
	

	)
}
