import React, { useState } from 'react';
import CardProduct from './CardProduct';

const SearchProduct = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/search`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName: searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
    } catch (error) {
      console.error('Error searching for Products:', error);
    }
  };



  return (
    <div className="pt-5 container">
      <h2>Search Products</h2>
      <div className="form-group">
        <label htmlFor="productName" className="mb-3">Product Name:</label>
        <input
          type="text"
          id="productName"
          className="form-control form-control-lg"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
        />
      </div>
      <button className="btn btn-primary my-4" onClick={handleSearch}>
        Search
      </button>
      <h3>Search Results:</h3>
      <ul className="d-flex justify-content-center">
        {searchResults.map(product => (
          <CardProduct productProp={product} key={product._id}/>
        ))}
      </ul>
    </div>
  )
};

export default SearchProduct;
