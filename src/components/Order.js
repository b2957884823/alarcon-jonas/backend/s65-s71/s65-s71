import {useEffect, useState, useContext} from 'react';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView';
import CreateOrder from './CreateOrder';

export default function Order() {

  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([])

  const fetchOrderData = () => {

    fetch(`${process.env.REACT_APP_API_URL}/orders/getAllOrders`)
    .then(res => res.json())
    .then(data => {
      console.log(data)
      setProducts(data);
    });
  }

  useEffect(() => {
    fetchProductData();
  }, []);


  return (

    <>
      {
        
      }
    </>

  )

}