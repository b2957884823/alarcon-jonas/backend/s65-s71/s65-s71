import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

const ResetPassword = ({ onPasswordReset }) => {
  const [currentPassword, setCurrentPassword] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [showReset, setShowReset] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const openReset = () => {
    setShowReset(true);
  };

  const closeReset = () => {
    setShowReset(false);
    setCurrentPassword('');
    setPassword('');
    setConfirmPassword('');
  };

  const handleResetPassword = async (e) => {
    e.preventDefault();

    if (password !== confirmPassword) {
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'New password and confirmation password do not match.',
      });
      return;
    }

    setIsLoading(true);

    const passwordData = {
      currentPassword,
      newPassword: password,
    };

    try {
      const token = localStorage.getItem('token');
      const response = await fetch(`${process.env.REACT_APP_API_URL}/users/reset-password`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(passwordData),
      });

      if (response.ok) {
        Swal.fire({
          title: 'Success!',
          icon: 'success',
          text: 'Password Successfully Updated',
        });
        setCurrentPassword('');
        setPassword('');
        setConfirmPassword('');
        closeReset();
        onPasswordReset();
      } else {
        const errorData = await response.json();
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: errorData.message || 'Password update failed. Please try again later.',
        });
      }
    } catch (error) {
      console.error('Error updating password:', error);
      Swal.fire({
        title: 'Error',
        icon: 'error',
        text: 'An error occurred. Please try again later.',
      });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      <Button size="sm" variant="outline-light" className="ButtonColor text-white" onClick={openReset}>
        Reset Password
      </Button>

      <Modal show={showReset} onHide={closeReset}>
        <Form onSubmit={handleResetPassword}>
          <Modal.Header closeButton>
            <Modal.Title>Reset Password</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="currentPassword">
              <Form.Label>Current Password</Form.Label>
              <Form.Control
                type="password"
                required
                value={currentPassword}
                onChange={(e) => setCurrentPassword(e.target.value)}
                autoComplete="current-password"
              />
            </Form.Group>

            <Form.Group controlId="password">
              <Form.Label>New Password</Form.Label>
              <Form.Control
                type="password"
                required
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                autoComplete="new-password"
              />
            </Form.Group>

            <Form.Group controlId="confirmPassword">
              <Form.Label>Confirm Password</Form.Label>
              <Form.Control
                type="password"
                required
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                autoComplete="new-password"
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeReset} disabled={isLoading}>
              Close
            </Button>
            <Button variant="success" type="submit" disabled={isLoading}>
              {isLoading ? 'Submitting...' : 'Submit'}
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default ResetPassword;
