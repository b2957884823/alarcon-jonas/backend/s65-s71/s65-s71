import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UpdateProfile({ onProfileUpdate }) {

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [showEdit, setShowEdit] = useState(false);

  const openEdit = () => {
    fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setFirstName(data.firstName);
        setLastName(data.lastName);
        setMobileNo(data.mobileNo);
      });

    setShowEdit(true);
  };

  const closeEdit = () => {
    setShowEdit(false);
    setFirstName('');
    setLastName('');
    setMobileNo('');
  };

  const editProfile = (e) => {
    e.preventDefault();

    const profileData = {
      firstName: firstName,
      lastName: lastName,
      mobileNo: mobileNo,
    };

    fetch(`${ process.env.REACT_APP_API_URL }/users/profile`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(profileData),
    })
      .then((res) => res.json())
      .then((response) => {
        if (response._id) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Profile Successfully Updated',
          });
          onProfileUpdate(profileData);
          setFirstName('');
          setLastName('');
          setMobileNo('');
          closeEdit();
        } else {
          Swal.fire({
            title: 'Error',
            icon: 'error',
            text: 'Profile update failed. Please try again later.',
          });
        }
      })
      .catch((error) => {
        console.error('Error updating profile:', error);
        Swal.fire({
          title: 'Error',
          icon: 'error',
          text: 'An error occurred. Please try again later.',
        });
      });
  };

  return (
    <>
      <Button size="sm" onClick={openEdit} variant="outline-light" className="ButtonColor text-white">
        Update Profile
      </Button>

      <Modal show={showEdit} onHide={closeEdit}>
        <Form onSubmit={editProfile}>
          <Modal.Header closeButton>
            <Modal.Title>Update Profile</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="firstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                required
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="lastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                required
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="mobileNo">
              <Form.Label>Mobile Number</Form.Label>
              <Form.Control
                type="text"
                required
                value={mobileNo}
                onChange={(e) => setMobileNo(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeEdit}>
              Close
            </Button>
            <Button variant="success" type="submit">
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
}
