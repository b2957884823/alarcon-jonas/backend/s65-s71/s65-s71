import { Row, Col, Card, Button, } from 'react-bootstrap';
import { useContext } from 'react'
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';



export default function CardProduct({productProp}) {
  const { breakPoint, data } = productProp
  const { user } = useContext(UserContext);

 	 if (!productProp) {
    return null;
  }


	// Destructured the productProp to access its properties.
	const {_id, name, description, price } = productProp;



return(

    <row xs={12} md={6} className="p-4 d-flex justify-content-center">
        <Col>
        <Card style={{ width: '15rem', height: '20rem' }}>
          <Card.Body>
            <Card.Title>
            <Link to={`/products/${_id}`}>{name}</Link>
            </Card.Title>
            <Card.Text>{description}</Card.Text>
          </Card.Body>

          <Card.Footer>
            <h6 className="text-center">₱ {price}</h6>
          </Card.Footer>
          {
                  (user.id === null)
                  ?
                  <Button as={Link} to="/login" className="ButtonColor text-white">Details</Button>
                  :
                  <Button as={Link} to={`/products/${_id}`} className="ButtonColor text-white">Details</Button>
                }
          </Card>
      </Col>
    </row>
    

    )

}