import { useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import CardProduct from '../components/CardProduct';
import SearchProduct from '../components/SearchProduct';





export default function UserView({productData, fetchData}) {

  const [product, setProduct] = useState([])

  useEffect(() => {
    const productArr = productData.map(product => {
      console.log(product)
      if(product.isAvailable === true) {
        return (
          <CardProduct productProp={product} key={product._id}/>
          )
      } else {
        return null;
      }
    })
    setProduct(productArr)

  }, [productData])




  return(
    <>
    
    <SearchProduct/>

      
     <Container className="mt-10">
  <Row>
    {product.map((product, index) => (
      <Col ms={1} md={4} key={index}>
        {product}
      </Col>
    ))}
  </Row>
</Container>


    
    </>
  )
}

