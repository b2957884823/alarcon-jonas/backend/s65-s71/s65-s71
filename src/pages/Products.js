import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';




export default function Products() {
  const { user } = useContext(UserContext);

  const [product, setProduct] = useState([]);
  const [loading, setLoading] = useState(true);

  // Create a function to fetch all products
  const fetchProductData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProduct(data);
        setLoading(false);
      });
  };



  useEffect(() => {
  fetchProductData();
  }, []);

  const fetchActiveProductData = () => {

    fetch(`${ process.env.REACT_APP_API_URL }/products/active`)
    .then(res => res.json())
    .then(data => {
      setProduct(data);
    });
  }

  useEffect(() => {
  fetchActiveProductData();
  }, []);

  return (
    <>
    
      {loading ? (
        <div>Loading...</div> 
      ) : (
        <>
          {user.isAdmin ? ( 
            <AdminView productData={product} fetchData={fetchProductData}/>
          ) : (
            // If the user is not an admin, show the Regular Products Page.
            <UserView productData={product} fetchActiveProductData={fetchActiveProductData}/>
          )}
        </>
      )}

    </>
  );
}
