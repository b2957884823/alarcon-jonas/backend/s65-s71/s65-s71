import { useState, useContext, useEffect } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import UpdateProfile from '../components/UpdateProfile';
import ResetPassword from '../components/ResetPassword';




export default function Profile(){

    const {user} = useContext(UserContext);

    const [details,setDetails] = useState({})

    useEffect(() => {
    fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      // Set the user states values with the user details upon successful login.
      if (typeof data._id !== "undefined") {

        setDetails(data);

      }
        });

    },[])

      const handleProfileUpdate = (updatedProfileData) => {
    setDetails(updatedProfileData);
  };


  const handlePasswordReset = () => {
      Swal.fire({
        title: 'Successful',
        icon: 'success',
        text: 'You successfully reset your password.',
      })
    };


  return (
        
        (user.id === null) ?
        <Navigate to="/" />
        :
    <>
        <Row>
          <Col className="p-5 bg-primary text-white">
            <h1 className="my-5 d-flex justify-content-center ">PROFILE</h1>
                    
            <h2 className="mt-3 text-white">{`${details.firstName} ${details.lastName}`}</h2>
            <hr />
            <h4 className="text-white">Contacts</h4>
            <ul>
              
                        <li className="text-white">Email: {details.email}</li>
               
              <li className="text-white">Mobile No: {details.mobileNo}</li>
            </ul>
          </Col>
        </Row>
<Container>
            <Row className="pt-4 mt-4">
              <Col className="d-flex justify-content-center">
                <Button style={{ marginRight: '20px' }}>
                  <UpdateProfile onProfileUpdate={handleProfileUpdate} />
                </Button>
                <Button style={{ marginRight: '20px' }}>
                  <ResetPassword onPasswordReset={handlePasswordReset}/>
                </Button>
                <Button >
                  <Button size="sm" as={Link} to="/orders/getOrders" variant="outline-light" className="ButtonColor text-white">Order History</Button>
                </Button>
              </Col>
            </Row>
          </Container>
   </>

  )

}
