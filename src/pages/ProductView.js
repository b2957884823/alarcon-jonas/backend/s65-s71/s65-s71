import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";
import UserContext from '../UserContext';

export default function ProductView() {

	const { productId } = useParams();
	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [quantity, setQuantity] = useState(1);
	const [price, setPrice] = useState(0);
	const [totalAmount, setTotalAmount] = useState(0);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/single/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

	const handleQuantityIncrease = () => {
	    setQuantity((prevQuantity) => prevQuantity + 1);
	  };

	  const handleQuantityDecrease = () => {
	    setQuantity((prevQuantity) => Math.max(prevQuantity - 1, 1));
	  };

	  useEffect(() => {
	    fetch(`${process.env.REACT_APP_API_URL}/products/single/${productId}`)
	      .then((res) => res.json())
	      .then((data) => {
	        setName(data.name);
	        setDescription(data.description);
	        setPrice(data.price);
	      })
	      .catch((error) => {
	        console.error('Error fetching product details:', error);
	      });
	  }, [productId]);

	  useEffect(() => {
	    setTotalAmount(price * quantity);
	  }, [price, quantity]);

	  const handleSubmitOrder = (e) => {
	      e.preventDefault();

	      if (user.isAdmin) {
	        Swal.fire({
	          title: 'Cannot buy as an Admin',
	          icon: 'error',
	          text: 'Admins cannot make a purchase.',
	        });
	        return;
	      }

	      if (quantity <= 0) {
	        Swal.fire({
	          title: 'Invalid Quantity',
	          icon: 'error',
	          text: 'Quantity must be greater than 0.',
	        });
	        return;
	      }






	      fetch(`${process.env.REACT_APP_API_URL}/orders/createOrder`, {
	        method: 'POST',
	        headers: {
	          'Content-Type': 'application/json',
	          Authorization: `Bearer ${localStorage.getItem('token')}`,
	        },
	        body: JSON.stringify({
	          productId: productId,
	          name: name,
	          quantity: quantity,
	        }),
	      })
	        .then((res) => {
	          if (!res.ok) {
	            throw new Error('Failed to place the order');
	          }
	          return res.json();
	        })
	        .then((data) => {
	          Swal.fire({
	            title: 'Order Placed Successfully',
	            icon: 'success',
	            text: 'Your order has been placed successfully.',
	          });

	          navigate('/products');
	        })
	        .catch((error) => {
	          console.error('Error placing the order:', error);
	          Swal.fire({
	            title: 'Error',
	            icon: 'error',
	            text: 'An error occurred while placing the order. Please try again later.',
	          });
	        });
	    };

	  return (
	    <Container className="mt-5">
			<Row className="justify-content-center">
				<Col xs={12} md={6} className="p-3">
					<Card className="cardHighlight CardColor">
						<Card.Body className="d-flex flex-column">
						  <Card.Title className="text-black text-center">{name}</Card.Title>
						  <Card.Text className="text-start text-black mt-3">{description}</Card.Text>
						  <div className="mt-auto text-start mb-2 text-black">₱ {price}</div>
						<Card.Subtitle className="text-black">Quantity:</Card.Subtitle>
	                	<div className="p-3 text-white">
	                		<Button variant="outline-dark" size="sm" onClick={handleQuantityDecrease} disabled={quantity <= 1}>-</Button>
	                		<span className="text-black">	{quantity}	</span>
	                		<Button variant="outline-dark" size="sm" onClick={handleQuantityIncrease}>+</Button>
	                	</div>
	                	<Card.Subtitle className="text-black">Total Amount: ₱ {totalAmount}</Card.Subtitle>
						</Card.Body>
						<Card.Footer>
						<Button  onClick={handleSubmitOrder} className="w-100 ButtonColor btn btn-primary">Checkout</Button>
						</Card.Footer>
					</Card>
				</Col>	
			</Row>
		</Container>
	  );
}