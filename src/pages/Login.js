import { useState, useEffect, useContext } from 'react'
import { Form, Button, Container } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login() {

	
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [ isActive , setIsActive ] = useState(false);


	function loginUser(e) {
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			

			if(data.access) {
			  localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Circle!"
				});

			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again."
				});
			}

		})
		
		setEmail('');
		setPassword('');
	};

	const retrieveUserDetails = (token) => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
		})
	}

	useEffect(() => {

		if(email !== "" && password !=="") {

			setIsActive(true)
		} else {

			setIsActive(false)
		}

	}, [email, password]);	




	return(

		(user.id !== null) ?
			<Navigate to="/profile" /> 
			:
			  <Container className="d-flex justify-content-center login" style={{ backgroundColor: '#EEEEEE' }}>
					<Form onSubmit={e => loginUser(e)} className="mt-5">
					<h1 className=" text-center">Login</h1>

				      <Form.Group className="mt-5 justify-content-center" controlId="Email">
				        <Form.Label>Email Address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	placeholder="Enter email" 
				        	required 
				        	value={email}
				        	onChange={e => {setEmail(e.target.value)}}
				        />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="Password1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        type="password" 
				        placeholder="Password" 
				        required 
				        value={password}
				        onChange={e => {setPassword(e.target.value)}}
				        />
				      </Form.Group>
				      <Form.Group className="d-flex justify-content-center">

				      {
				      	isActive 
				      		? <Button variant="success" type="submit" id="submitBtn">Login</Button>
				      		: <Button variant="danger" type="submit" id="submitBtn" disabled>Login</Button>
				      }
				      </Form.Group>

			    	</Form>
			   </Container>

	)
}
