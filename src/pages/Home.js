import Banner from '../components/Banner';
// import CardProduct from '../components/CardProduct'
import Highlights from '../components/Highlights';
import FeaturedProduct from '../components/FeaturedProduct'



export default function Home() {

	return (
		<>
			<Banner/>
			<FeaturedProduct/>
			<Highlights/>
			{/*<CardProduct/>*/}
		</>
	)

};